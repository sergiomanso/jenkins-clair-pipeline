# Security Scanning with Clair in a Jenkins pipeline

This solution aims to provide a functional Jenkins deployment with a pre-configured pipeline using Ansible. This pipeline is triggered by pushes on a GitLab repository. Firstly, it clones the repository (Springboot Web Service with respective Dockerfile), builds a Docker image locally, scans the built Docker image for vulnerabilities, and, after some decision making, the image is pushed or not to DockerHub.

### Arquitecture

![](images/arch.png)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Virtual Machine running Ubuntu 18.04 (e.g. AWS, CGP, DigitalOcean)

Host to launch the project which should have installed the following packages:

```
Python3 (tested with 3.8.0)
ansible==2.9.2
```

### Configuration

Inside 'playbooks' folder, configure the provided 'hosts' inventory file by listing the virtual machine with the respective credentials and IP address.

```
[pipeline]
jenkins ansible_host=<IP_ADDRESS> ansible_user=<REMOTE_USER> ansible_ssh_extra_args='-o StrictHostKeyChecking=no' ansible_ssh_private_key_file=<PATH_TO_PRIVATE_KEY>
```

### Deployment

Run the playbook with:

```
cd playbooks && ansible-playbook deploy.yml -i hosts
```

After the process is completed, Jenkis will be available at

```
http://IP_ADDRESS:8080

Username: admin
Password: rhotask
```

### Tests

The Docker image scanning stage uses Clair to detect vulnerabilities in the built Docker image and it is configured to stop the building process when CVE's with 'HIGH' severity are found and when other CVE's defined in a policy file are also found.

This pipeline is configured to approve the vulnerabilities found in the Docker image built from the repository as the following output shows.

```
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| STATUS   | CVE SEVERITY          | PACKAGE NAME  | PACKAGE VERSION | CVE DESCRIPTION                                                 |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | High CVE-2018-1000654 | libtasn1      | 4.13-r0         |                                                                 |
|          |                       |               |                 | https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1000654 |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | High CVE-2019-14697   | musl          | 1.1.20-r4       |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14697 |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | Medium CVE-2018-14498 | libjpeg-turbo | 1.5.3-r4        |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14498 |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | Medium CVE-2019-2762  | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2762  |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | Medium CVE-2019-2769  | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2769  |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | Medium CVE-2019-2816  | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2816  |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | Medium CVE-2019-2842  | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2842  |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | Low CVE-2019-2745     | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2745  |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | Low CVE-2019-7317     | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-7317  |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | Low CVE-2019-2786     | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2786  |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved | Low CVE-2019-2766     | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2766  |
+----------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
```

The approval of this Docker image is achieved by setting the treshold of Clair to 'High' and by setting the two High severity CVE's (CVE-2018-1000654 and High CVE-2019-14697) in the whitelist of the policy file.

If these CVE's are removed from the whitelist, the building proccess is aborted and the image is not submitted to Docker Hub. The following output represents this scenario:

```
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| STATUS     | CVE SEVERITY          | PACKAGE NAME  | PACKAGE VERSION | CVE DESCRIPTION                                                 |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Unapproved | High CVE-2018-1000654 | libtasn1      | 4.13-r0         |                                                                 |
|            |                       |               |                 | https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1000654 |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Unapproved | High CVE-2019-14697   | musl          | 1.1.20-r4       |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14697 |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved   | Medium CVE-2018-14498 | libjpeg-turbo | 1.5.3-r4        |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14498 |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved   | Medium CVE-2019-2762  | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2762  |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved   | Medium CVE-2019-2769  | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2769  |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved   | Medium CVE-2019-2816  | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2816  |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved   | Medium CVE-2019-2842  | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2842  |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved   | Low CVE-2019-2745     | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2745  |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved   | Low CVE-2019-7317     | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-7317  |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved   | Low CVE-2019-2786     | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2786  |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
| Approved   | Low CVE-2019-2766     | openjdk8      | 8.212.04-r0     |   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-2766  |
+------------+-----------------------+---------------+-----------------+-----------------------------------------------------------------+
```

The policy file should be configured according to the needs. This file is located in

```
cat /var/lib/jenkins/workspace/policies/policy.yml

generalwhitelist: #Approve CVE for any image
  CVE-2019-14697: musl
  CVE-2018-1000654: libtasn1
images:
  ubuntu: #Approve CVE only for ubuntu image, regardles of the version.
    CVE-2017-5230: Java
    CVE-2017-5230: XSX
  alpine:
    CVE-2017-3261: SE 
```

Clair threshold can be modified in the jenkinsfile (Pipeline script) via

```
http://IP_ADDRESS:8080/job/docker-auditory/configure
```

Example:
```
  ...
  (line 38) ./clair-scanner -c http://$CLAIR:6060 --ip="$DOCKER_GATEWAY" -w ../policies/policy.yml **--threshold="High"** rhotask/springbootws:$BUILD_NUMBER || exit 1
  ...
```

## Authors

* **Sérgio Manso**
