#!groovy
import jenkins.model.*;
import hudson.security.*;
import com.cloudbees.plugins.credentials.impl.*;
import com.cloudbees.plugins.credentials.*;
import com.cloudbees.plugins.credentials.domains.*;
import hudson.util.Secret;
import com.dabsquared.gitlabjenkins.connection.*;

def gitlabToken = 'g6sXnCUtjKrmm-GAHpwT'

def system_credentials_provider = SystemCredentialsProvider.getInstance()
def credentialDescription = "Gitlab Integration token"
def credentialScope = CredentialsScope.GLOBAL
def credentialsId = "gitlab-secrets-id"
def credential_domain = com.cloudbees.plugins.credentials.domains.Domain.global()
def credential_creds = new GitLabApiTokenImpl(credentialScope,credentialsId,credentialDescription,Secret.fromString(gitlabToken))

gitlab_credentials_exist = false
system_credentials_provider.getCredentials().each {
  credentials = (com.cloudbees.plugins.credentials.Credentials) it
  if ( credentials.getDescription() == credentialDescription) {
    gitlab_credentials_exist = true
    println("Found existing credentials: " + credentialDescription)
    system_credentials_provider.removeCredentials(credential_domain,credential_creds)
    println(credentialDescription + " is removed and will be recreated..")
  }
}

println "--> Registering Gitlab API token.."
system_credentials_provider.addCredentials(credential_domain,credential_creds)
println(credentialDescription + " created..")
