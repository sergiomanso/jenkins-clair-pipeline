#!groovy
import jenkins.model.*;
import hudson.security.*;
import com.cloudbees.plugins.credentials.impl.*;
import com.cloudbees.plugins.credentials.*;
import com.cloudbees.plugins.credentials.domains.*;
import hudson.util.Secret;
import com.dabsquared.gitlabjenkins.connection.*;

def jenkinsKeyUsernameWithPasswordParameters = [
  description:  'docker hub rho task credentials',
  id:           'dockerhub',
  secret:       '@Stuff001',
  userName:     'rhotask'
]

Jenkins jenkins = Jenkins.getInstance()
def domain = Domain.global()
def store = jenkins.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()

def jenkinsKeyUsernameWithPassword = new UsernamePasswordCredentialsImpl(
  CredentialsScope.GLOBAL,
  jenkinsKeyUsernameWithPasswordParameters.id,
  jenkinsKeyUsernameWithPasswordParameters.description,
  jenkinsKeyUsernameWithPasswordParameters.userName,
  jenkinsKeyUsernameWithPasswordParameters.secret
)

store.addCredentials(domain, jenkinsKeyUsernameWithPassword)
jenkins.save()