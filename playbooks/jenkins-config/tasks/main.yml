---
- name: Add user Jenkins to the docker group
  user:
    name:  "jenkins"
    groups: docker
    append: yes 

- name: Waiting for apt to be available
  become: yes
  shell: while sudo fuser /var/lib/dpkg/lock >/dev/null 2>&1; do sleep 1; done;

- name: "install dependencies"
  apt:
    name: "{{ item }}"
  with_items:
    - jq
    - python-pip
    - python-minimal
    - python3-pip

- name: "install python dependencies"
  pip:
    name: "{{ item }}"
    executable: pip2
  with_items:
    - python-jenkins
    - lxml

- name: Getting crumb
  shell: CRUMB=$(curl -sS --cookie-jar ./cookie 'http://{{ jenkins_admin_username }}:{{ jenkins_admin_password }}@{{ jenkins_hostname }}:{{ jenkins_http_port }}/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)') &&
    curl -sS --cookie ./cookie -H $CRUMB 'http://{{ jenkins_hostname }}:{{ jenkins_http_port }}/me/descriptorByName/jenkins.security.ApiTokenProperty/generateNewToken' -X POST --data 'newTokenName=admin-token' --user {{ jenkins_admin_username }}:{{ jenkins_admin_password }} | jq .
  register: output

- set_fact:
    admin_token: "{{ (output.stdout|from_json).data.tokenValue }}"  

- name: fix Jenkins wizard
  shell: sed -i 's#<installStateName>NEW.*#<installStateName>RUNNING<\/installStateName>#g' /var/lib/jenkins/config.xml && service jenkins restart

- name: Wait for Jenkins to start up before proceeding.
  command: >
    curl -D - --silent --max-time 5 http://{{ jenkins_hostname }}:{{ jenkins_http_port }}{{ jenkins_url_prefix }}/cli/

- name: Config gitlab token
  jenkins_script:
    script: "{{ lookup('file', 'gitlab_token.groovy') }}"
    user: "{{ jenkins_admin_username }}"
    password: "{{ admin_token }}"
    url: http://{{ jenkins_hostname }}:{{ jenkins_http_port }}
  register: result
  retries: 10
  delay: 3
  until: result is succeeded
  ignore_errors: yes

- name: Config Docker Hub creds
  jenkins_script:
    script: "{{ lookup('file', 'files/dockerhub_creds.groovy') }}"
    user: "{{ jenkins_admin_username }}"
    password: "{{ admin_token }}"
    url: http://{{ jenkins_hostname }}:{{ jenkins_http_port }}


- name: Config gitlab plugin
  template:
    src: com.dabsquared.gitlabjenkins.connection.GitLabConnectionConfig.xml.j2
    dest: /var/lib/jenkins/com.dabsquared.gitlabjenkins.connection.GitLabConnectionConfig.xml
    owner: jenkins
    group: jenkins
    mode: '0644'
    
- name: Set the python interpreter to the symlink (which points at version 2.7)
  set_fact:
    ansible_python_interpreter: '/usr/bin/python'

- name: Create job with pipeline
  jenkins_job:
    config: "{{ lookup('file', 'files/config.xml') }}"
    name: "{{ project }}"
    user: "{{ jenkins_admin_username }}"
    password: "{{ admin_token }}"
    url: http://{{ jenkins_hostname }}:{{ jenkins_http_port }}

- name: Set the python interpreter back to version 3
  set_fact:
    ansible_python_interpreter: '/usr/bin/python3'

- name: Setup gitlab service integration with jenkins
  shell: "curl -X PUT -d 'jenkins_url=http://{{ ansible_default_ipv4.address }}:{{ jenkins_http_port }}&project_name={{ project }}&username={{ jenkins_admin_username }}&password={{ jenkins_admin_password }}' https://gitlab.com/api/v4/projects/15963444/services/jenkins -H 'PRIVATE-TOKEN:kSJFvAUp8yiw8T_ZFgHb'"

- name: Creates policies directory
  file:
    path: /var/lib/jenkins/workspace/policies
    state: directory
    owner: jenkins
    group: jenkins
    mode: '0755'

- name: Copy policy file
  template:
    src: ./files/policy.yml
    dest: /var/lib/jenkins/workspace/policies/policy.yml
    owner: jenkins
    group: jenkins
    mode: '0644'

- name: "install python dependencies"
  pip:
    name: "{{ item }}"
  with_items:
    - docker
    - docker-compose

- name: Creates clair directory
  file:
    path: clair
    state: directory

- name: Copy compose source templates
  template:
    src: ./clair/docker-compose.yml
    dest: "clair/docker-compose.yml"
    
- name: Create and start services
  docker_compose:
    project_src: "~/clair"

- name: Enable build trigger "on push"
  shell: curl -X POST http://{{ jenkins_admin_username }}:{{ admin_token }}@{{ jenkins_hostname }}:{{ jenkins_http_port }}/job/{{ project }}/build

